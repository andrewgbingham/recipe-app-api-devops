resource "aws_s3_bucket" "app_public_files" {
  bucket        = "devops-project-files"
  acl           = "private"
  force_destroy = true
}